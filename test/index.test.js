import lib, { template } from '../src/index.js';
import {jest} from '@jest/globals'

test('Example test', ()=>{
	const fn = jest.fn((literals, params)=>{
		return {
			literals,
			sql: literals.sql,
			text: literals.text,
			params,
		};
	});

	const tag = lib(fn);

	expect(tag`One ${'two'} (${[3,4]})
	VALUES ${[[5,6],[7,8]]}
	${tag('nine')}
	${sql=>sql`ten ${10} ${sql('x')}`}`).toMatchSnapshot()
})

test('Template', () => {
	expect(template`One ${2}; ${[3,4]}`((...x)=>x)).toMatchSnapshot();
})
