// Todo: Allow browser/Deno usage
// Depends on a non-node version of sql-template-tag being available
import { sqltag, raw, join } from 'sql-template-tag';

function transformParameter(param){
	if(param instanceof Function) {
		return param(flatten)

		// if(Array.isArray(result)) return join(result, '') ???
	}

	if(Array.isArray(param)) {
		if(Array.isArray(param[0])) {
			return join(
				param.map(values =>
					sqltag`(${join(values)})`
				)
			)
		} else {
			return join(param)
		}
	} else {
		return param
	}
}

const flatten = withRawHelper(function (literals, ...params) {
	params = params.map(transformParameter);

	return sqltag(literals, ...params);
})

function withRawHelper(method) {
	return (...args) => {
		if(!Array.isArray(args[0])) {
			return raw(args[0]);
		}

		return method(...args);
	}
}

export default execute => {
	return withRawHelper((literals, ...params) => {
		const t = flatten(literals, ...params);

		const strings = t.strings;
		strings.text = t.text;
		strings.sql = t.sql;
		strings.values = t.values;

		return execute(t.strings, t.values)
	})
}

export const template = withRawHelper((...args)=>sql=>sql(...args))
